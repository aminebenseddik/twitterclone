<?php

namespace Corp\TwitterBundle\Controller;

use Corp\TwitterBundle\Entity\Tweet;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class TweetController extends Controller
{
    public function indexAction()
    {
        $tweets = $this->getDoctrine()
                    ->getRepository('CorpTwitterBundle:Tweet')
                    ->findAll();

        dump($tweets);

        return $this->render('CorpTwitterBundle:Tweet:index.html.twig',
                array(
                    'tweets' => $tweets
                )
            );
    }

    public function addAction(Request $request)
    {
        $tweet = new Tweet();

        $form = $this->createFormBuilder($tweet)
            ->add('content', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Send Tweet'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tweet = $form->getData();
            $user = $this->getUser();
            $date = new \DateTime();

            $tweet->setUser($user);
            $tweet->setPublishedAt($date);

            dump($tweet);
            dump($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($tweet);
            $em->flush();

            return $this->redirect(
                $this->generateUrl('corp_twitter_home_page')
            );
        }


        return $this->render('CorpTwitterBundle:Tweet:add.html.twig',
            array(
                'form' => $form->createView()
            ));
    }
}
